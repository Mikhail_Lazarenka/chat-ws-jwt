package com.ubldk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatWsAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChatWsAuthApplication.class, args);
    }

}
